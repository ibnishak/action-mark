/*\
title: $:/tesseract/modules/widgets/action-mark.js
type: application/javascript
module-type: widget

Action widget to surround the selected text with any html element without going to edit mode. 
\*/
(function() {

    /*jslint node: true, browser: true */
    /*global $tw: false */
    /*"use strict";*/

    var Widget = require("$:/core/modules/widgets/widget.js").widget;

    var SetFieldWidget = function(parseTreeNode, options) {
        this.initialise(parseTreeNode, options);
    };

    /*
    Inherit from the base widget class
    */
    SetFieldWidget.prototype = new Widget();

    /*
    Render this widget into the DOM
    */
    SetFieldWidget.prototype.render = function(parent, nextSibling) {
        this.computeAttributes();
        this.execute();
    };

    /*
    Compute the internal state of the widget
    */
    SetFieldWidget.prototype.execute = function() {
        this.element = this.getAttribute("$element");
    };
    /*
    Refresh the widget by ensuring our attributes are up to date
    */
    SetFieldWidget.prototype.refresh = function(changedTiddlers) {
        var changedAttributes = this.computeAttributes();
        if (changedAttributes["$tiddler"] || changedAttributes["$field"] || changedAttributes["$index"] || changedAttributes["$value"]) {
            this.refreshSelf();
            return true;
        }
        return this.refreshChildren(changedTiddlers);
    };
    /*
    Invoke the action associated with this widget
    */
    SetFieldWidget.prototype.invokeAction = function(triggeringWidget, event) {
        var tiddlertitle = this.getVariable("currentTiddler");

        var userSelection = window.getSelection().getRangeAt(0);
        var safeRanges = getSafeRanges(userSelection);
        for (var i = 0; i < safeRanges.length; i++) {
            highlightRange(safeRanges[i], this.element, this.attributes);
        }
        var tiddlertext = document.querySelector("[data-tiddler-title=" + CSS.escape(tiddlertitle) + "]").getElementsByClassName("tc-tiddler-body")[0].innerHTML
        var self = this,
            options = {};
        this.wiki.setText(tiddlertitle, "text", null, tiddlertext, options);
        return true; // Action was invoked
    };

    exports["action-mark"] = SetFieldWidget;

})();

function highlightRange(range, element, attributes) {
    var newNode = document.createElement(element);
    $tw.utils.each(attributes, function(attribute, name) {
        if (name.charAt(0) !== "$") {
            newNode.setAttribute(name, attribute)
        }
    });
    range.surroundContents(newNode);
}

function getSafeRanges(dangerous) {
    var a = dangerous.commonAncestorContainer;
    // Starts -- Work inward from the start, selecting the largest safe range
    var s = new Array(0),
        rs = new Array(0);
    if (dangerous.startContainer != a)
        for (var i = dangerous.startContainer; i != a; i = i.parentNode)
            s.push(i);
    if (0 < s.length)
        for (var i = 0; i < s.length; i++) {
            var xs = document.createRange();
            if (i) {
                xs.setStartAfter(s[i - 1]);
                xs.setEndAfter(s[i].lastChild);
            } else {
                xs.setStart(s[i], dangerous.startOffset);
                xs.setEndAfter(
                    (s[i].nodeType == Node.TEXT_NODE) ?
                    s[i] : s[i].lastChild
                );
            }
            rs.push(xs);
        }

    // Ends -- basically the same code reversed
    var e = new Array(0),
        re = new Array(0);
    if (dangerous.endContainer != a)
        for (var i = dangerous.endContainer; i != a; i = i.parentNode)
            e.push(i);
    if (0 < e.length)
        for (var i = 0; i < e.length; i++) {
            var xe = document.createRange();
            if (i) {
                xe.setStartBefore(e[i].firstChild);
                xe.setEndBefore(e[i - 1]);
            } else {
                xe.setStartBefore(
                    (e[i].nodeType == Node.TEXT_NODE) ?
                    e[i] : e[i].firstChild
                );
                xe.setEnd(e[i], dangerous.endOffset);
            }
            re.unshift(xe);
        }

    // Middle -- the uncaptured middle
    if ((0 < s.length) && (0 < e.length)) {
        var xm = document.createRange();
        xm.setStartAfter(s[s.length - 1]);
        xm.setEndBefore(e[e.length - 1]);
    } else {
        return [dangerous];
    }

    // Concat
    rs.push(xm);
    response = rs.concat(re);

    // Send to Console
    return response;
}