* Installing plugin

Create new tiddlywiki and its plugins folder
#+BEGIN_SRC
tiddlywiki mynewwiki --init server
cd mynewwiki
mkdir plugins
mkdir tiddlers
#+END_SRC

Clone the repo and symlink as shown below
#+BEGIN_SRC
git clone https://gitlab.com/ibnishak/action-mark.git
pushd plugins
ln -s ../action-mark/mark
popd
pushd tiddlers
ln -s ../action-mark/mark-helpers
popd
#+END_SRC

Start wiki
#+BEGIN_SRC
tiddlywiki --listen
#+END_SRC

